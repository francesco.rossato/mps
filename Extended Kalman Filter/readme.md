# Extended Kalman filter

This folder contains the implementation of the extended Kalman filter for the GPS position computation. 

The file `Extended_KF.m` contains the implementation of the actual generic Kalman filter, while the file `kalman_filter_main.m` contains the necessary code to compute the GPS position of the receiver.
It makes use of both Kalman filter and least square approximation and it compares the accuracy of the results, providing some plots.
The file `kalman_filter_test.m` is used to perform some tests to ensure that the code is working properly and providing the expected results. All the plots that this sctipt provide should converge (and in fact do converge) to the desired value indicated with a black horizontal line.

### Input files
- `pseudoranges.mat` contains the measured pseudorange from each satellite
- `sat_pos.mat` contains the position of each satellite

The other `.m` files contain some auxiliary functions.

## References
References for the general Kalman Filtering and KF with application to target tracking:
- [1] Y. Bar-Shalom, X. R. Li and T. Kirubarajan, Estimation with Applications to Tracking and Navigation, Wiley Interscience, 2001.
- [2] R. G. Brown and P. Y. C. Gwang, Introduction to Random Signals and
Applied Kalman Filtering, 3rd ed. New York: Wiley, 1997.
- [3] http://www.cs.unc.edu/~welch/kalman/


